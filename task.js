<script>
    $(document).ready(function () {
        $("#userSlider").on("input change", function () {
            var selectedUsers = parseInt($(this).val());

            // Determine which pricing plan to highlight based on selectedUsers
            if (selectedUsers >= 0 && selectedUsers <= 10) {
                $(".pricing-card").removeClass("highlight");
                $("#plan1").addClass("highlight");
            } else if (selectedUsers > 10 && selectedUsers <= 20) {
                $(".pricing-card").removeClass("highlight");
                $("#plan2").addClass("highlight");
            } // Add more conditions for other plans as needed
        });
    });
</script>
